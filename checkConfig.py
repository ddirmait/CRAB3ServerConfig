#!/usr/bin/python
import json
import sys

if len(sys.argv) > 1:
    cfgFile=sys.argv[1]
else:
    cfgFile='cmsweb-rest-config.json'

print("------------------ Testing external config: %s ----------------------\n\n\n" % cfgFile)
json.loads(open(cfgFile).read())

print("------------------ OK ----------------------" )
